import 'package:flutter/material.dart';

class CounterProvider extends ChangeNotifier {
  var _counter = 0;

  int get count => _counter;

  void incrementCount() {
    _counter += 1;
    notifyListeners();
  }

  void decrementCount() {
    _counter -= 1;
    notifyListeners();
  }
}
